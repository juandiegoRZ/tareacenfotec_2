#include <iostream>

struct Cliente
{
    int cedula;
    int telefono;
    std::string nombre;
    std::string *ptr_nombre = &nombre;
    std::string empresa;
    std::string *prt_empresa = &empresa;
} cliente, *ptr_cliente = &cliente;

void InsertarCliente(struct Cliente *ptr_cliente ,int cantidad_cliente, std::string nombre_cliente, int cedula_cliente, int telefono_cliente, std::string empresa_cliente)
{    
    ptr_cliente[cantidad_cliente].nombre = nombre_cliente;
    ptr_cliente[cantidad_cliente].cedula = cedula_cliente;
    ptr_cliente[cantidad_cliente].telefono = telefono_cliente;
    ptr_cliente[cantidad_cliente].empresa = empresa_cliente;
}

void MostrarClientes(Cliente *ptr_cliente, int clientes_ingresados)
{
    for (size_t i = 0; i < clientes_ingresados; i++)
    {
        std::cout << "-----------------" << "\n";
        std::cout << "Información de cliente #: " << i << std::endl;
        std::cout << "Nombre: " << (ptr_cliente + i)->nombre << std::endl;
        std::cout << "Cedula: " << (ptr_cliente + i)->cedula << std::endl;
        std::cout << "Telefono: " << (ptr_cliente + i)->telefono << std::endl;
        std::cout << "Empresa: " << (ptr_cliente + i)->empresa << std::endl;
    }
}

void BuscarCliente(Cliente *ptr_cliente, int cedula) 
{
    for (size_t i = 0; i < sizeof(ptr_cliente); i++)
    {
        if (ptr_cliente[i].cedula == cedula)
        {
            std::cout << "Cliente encontrado" << std::endl;
            std::cout << "Nombre: " << ptr_cliente[i].nombre << std::endl;
            std::cout << "Cedula: " << ptr_cliente[i].cedula << std::endl;
            std::cout << "Telefono: " << ptr_cliente[i].telefono << std::endl;
            std::cout << "Empresa: " << ptr_cliente[i].empresa << std::endl;
            return;
        }        
    }
    std::cout << "Cliente no encontrado" << std::endl;
    return;
}

void LiberarMemoria(Cliente *ptr_cliente) 
{
    //Se está dando un error en esta parte
    for (size_t i = 0; i < sizeof(ptr_cliente); i++)
    {
        delete (ptr_cliente + i)->ptr_nombre;
        delete (ptr_cliente + i)->prt_empresa; 
    }
    delete ptr_cliente;
    std::cout << "Memoria liberada" << std::endl;
}

int main()
{
    int cantidad_cliente = 0;
    std::string nombre;
    std::string *ptr_nombre = &nombre;
    int cedula = 0;
    int telefono = 0;
    std::string empresa;
    Cliente *ptr_array_clientes = new Cliente[cantidad_cliente];
    ptr_array_clientes = &cliente;

    std::cout << "Indique la cantidad de clientes que desea ingresar: " << std::endl;
    std::cin >> cantidad_cliente;
    int contador_cliente = 0;
    while (cantidad_cliente > contador_cliente)
    {
        std::cout << "Indique un nombre: " << std::endl;
        std::cin >> nombre;
        std::cout << "Indique cédula: " << std::endl;
        std::cin >> cedula;
        std::cout << "Indique un número de teléfono: " << std::endl;
        std::cin >> telefono;
        std::cout << "Indique un nombre de empresa" << std::endl;
        std::cin >> empresa;
        InsertarCliente(ptr_array_clientes, contador_cliente, nombre, cedula, telefono, empresa);
        contador_cliente++;
    }

    int mostrar_clientes = 0;
    std::cout << "¿Desea mostrar una lista completa de los clientes? \n Seleccione una opción: " << std::endl;
    std::cout << "1. Mostrar lista" << std::endl;
    std::cout << "2. Continuar programa" << std::endl;
    std::cin >> mostrar_clientes;
    
    if (mostrar_clientes == 1)
    {
       MostrarClientes(ptr_cliente, cantidad_cliente);
    }

    std::cout << "Indique el número de cédula que desea consultar:" << std::endl;
    std::cin >> cedula;

    BuscarCliente(ptr_array_clientes, cedula);

    int eliminar_clientes = 0;
    std::cout << "¿Desea elimiar todos los clientes? \n Seleccione una opción: " << std::endl;
    std::cout << "1. Eliminar clientes." << std::endl;
    std::cout << "2. Terminar programa." << std::endl;
    std::cin >> eliminar_clientes;

    if (eliminar_clientes == 1)
    {
        LiberarMemoria(ptr_cliente);
    }
    
    return 0;
}