#include <iostream>
/*Declare una variable números como un vector de 10 enteros. Escriba una función que recorra el
vector y modifique todos los enteros negativos cambiándole de signo. No se debe usar el
operador [], es decir, se deberá usar aritmética de punteros.
Nota: Recuerde que vector o array, son representaciones de punteros, por lo que debe
utilizar punteros a direcciones de memoria.
Recuerde el uso de new y delete.*/

void ModificarNegativos(int *vector, int largoVector = 0) {
    if (largoVector < 10)
    {               
        if (*(vector + largoVector) < 0)
        {
            *(vector + largoVector) = *(vector + largoVector) * -1;
        }
        largoVector++; 
        ModificarNegativos(vector, largoVector);
    }
    else
    {
        return;
    }
}

int main(int argc, char const *argv[])
{
    int *vector = new int[10]{-26, 56, -100, 589, 548, -1500, -4569, 5226, -948, 4569};
    std::cout << "Número antes de convertir los negativos a positivos" << std::endl;
    std::cout << *(vector) << ", " 
    << *(vector + 1) << ", " 
    << *(vector + 2) << ", " 
    << *(vector + 3) << ", " 
    << *(vector + 4) << ", " 
    << *(vector + 5) << ", " 
    << *(vector + 6) << ", " 
    << *(vector + 7) << ", " 
    << *(vector + 8) << ", " 
    << *(vector + 9) << std::endl;
    ModificarNegativos(vector);
    std::cout << "Números después de los cambios" << std::endl;
    std::cout << *(vector) << ", " 
    << *(vector + 1) << ", " 
    << *(vector + 2) << ", " 
    << *(vector + 3) << ", " 
    << *(vector + 4) << ", " 
    << *(vector + 5) << ", " 
    << *(vector + 6) << ", " 
    << *(vector + 7) << ", " 
    << *(vector + 8) << ", " 
    << *(vector + 9) << std::endl;
    delete[] vector;
    return 0;
}