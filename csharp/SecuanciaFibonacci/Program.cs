﻿/*Investigue sobre la Sucesión de Fibonacci, Fibonacci sigue la siguiente secuencia de números:
1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, ...
Desarrolle una función que calcule la secuencia de fibonacci para un número dado, ejemplo:
fibonacci(0) -> Resultado: 0
fibonacci(1) -> Resultado: 1
fibonacci(2) -> Resultado: 1
fibonacci(3) -> Resultado: 2
fibonacci(4) -> Resultado: 3
fibonacci(5) -> Resultado: 5
fibonacci(6) -> Resultado: 8
fibonacci(7) -> Resultado: 13
fibonacci(8) -> Resultado: 21
...
Nota: No debe utilizar ciclos, recuerde que las funciones pueden ser llamadas a sí mismas.
Cuando el número de fibonacci es 0, el resultado es 0, (nota: si el valor es 1, el resultado será 1)*/
internal class Program
{
    private static void Main(string[] args)
    {
        int SecuenciaFibonacci(int numero)
        {
            int resultado;
            if (numero == 0)
            {
                resultado = 0;
            }
            else if (numero == 1)
            {
                resultado = 1;
            }
            else
            {
                resultado = SecuenciaFibonacci(numero - 1) + SecuenciaFibonacci(numero - 2);
            }
            return resultado;
        }

        int pruebaFibonacci = 0;
        int resultadoFibonacci = 0;
        
        Console.WriteLine("Indique el número con el que desea iniciar la secuencia Fibonacci.");
        pruebaFibonacci = Convert.ToInt32(Console.ReadLine());
        resultadoFibonacci = SecuenciaFibonacci(pruebaFibonacci);
        Console.WriteLine($"El siguiente número después de {pruebaFibonacci} según la secuencia Fibonacci es: {resultadoFibonacci}");
    }
}